const register = {
  title: 'Choose your Country',
  text: 'If you’re already have an account please Login',
  textNumber: 'หมายเลขโทรศัพท์ มีการลงทะเบียนแล้ว',
  contactNumber: 'Contact Number',
  referance: 'Referrall (Optional)',
  continue: 'Continue',
  thai: 'Thailand',
  laos: 'Laos',
  username: 'Username',
  password: 'Password',
  confirmPassword: 'Confirm Password',
  bankName: 'Please Select',
  bankAccount: 'Bank Account',
  bankNo: 'Bank No',
  currency: 'Currency',
  code: 'Code',
  success: 'Congratulations',
  successText: "You've created your account successfully",
  btnDeposit: 'Click here to Deposit'
}

export default register
