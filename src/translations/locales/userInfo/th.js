const userInfo = {
  welcome: 'ยินดีต้อนรับ',
  member: 'สมาชิก',
  balance: 'ยอดเงิน',
  totalbonus: 'โบนัสทั้งหมด',
  lastbonus: 'โบนัสครั้งสุดท้าย',
  basicinfo: 'ข้อมูลพื้นฐาน',
  changepassword: 'เปลี่ยนรหัสผ่าน',
  diposit: 'เงินฝาก',
  withdraw: 'ถอนเงิน',
  tranfer: 'โอน',
  statement: 'ประวัติ',
  bonus: 'โบนัส',
  betreport: 'รายงานการเดิมพัน',
  turnoverchecking: 'การตรวจสอบผลประกอบการ',
  achievement: 'Achievement',
  achievementreport: 'Achievement report',
  logout: 'ออกจากระบบ',
  turnovercheckingDesktop: 'ผลประกอบการ',
  achievementDesktop: 'โบนัสสะสมยอดเทิร์น',
  forgotpass: 'ลืมรหัสผ่าน?'
}

export default userInfo
