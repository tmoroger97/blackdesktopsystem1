const userInfo = {
  welcome: 'Welcome',
  member: 'Member',
  balance: 'Balance',
  totalbonus: 'Total Bonus',
  lastbonus: 'Last Bonus',
  basicinfo: 'Basic Infomation',
  changepassword: 'Change Password',
  diposit: 'Diposit',
  withdraw: 'Withdraw',
  tranfer: 'Tranfer',
  statement: 'Statement',
  bonus: 'Bonus',
  betreport: 'Bet Report',
  turnoverchecking: 'Turnover Checking',
  achievement: 'Achievement',
  achievementreport: 'Achievement report',
  logout: 'Logout',
  turnovercheckingDesktop: 'Checking Turnover',
  achievementDesktop: 'Achievement',
  forgotpass: 'Forgot password?'
}

export default userInfo
