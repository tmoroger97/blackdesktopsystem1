const alert = {
  textAlertLogin: 'กรุณากรอกให้ครบ ชื่อผู้ใช้ และ รหัสผ่าน',
  textRegisNew: 'Register a new account',
  textFormatDate: 'รูปแบบวันที่ไม่ถูกต้อง',
  textAlertInputPass: 'กรุณากรอกรหัสผ่าน',
  textAlertPasswordIncorrect: 'รหัสผ่านไม่ตรงกัน',
  textAlertPasswordNot: 'รหัสผ่านปัจจุบันไม่ถูกต้อง'
}

export default alert
