const alert = {
  textAlertLogin: 'Please fill out the username and password.',
  textRegisNew: 'Register a new account',
  textFormatDate: 'Date Format Incorrect',
  textAlertInputPass: 'Please Input Password',
  textAlertPasswordIncorrect: 'Password Not Match',
  textAlertPasswordNot: 'Current Password Incorrect'
}

export default alert
