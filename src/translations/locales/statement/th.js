const statement = {
  statement: 'รายงานทางการเงิน',
  statementRecord: 'บันทึกคำชี้แจง',
  today: 'ในวันนี้',
  thisWeek: 'ในสัปดาห์นี้',
  lastWeek: 'เมื่อสัปดาห์ที่แล้ว',
  thisMonth: 'เดือนนี้',
  lastMonth: 'เดือนที่แล้ว',
  startDate: 'วันที่เริ่มต้น',
  endDate: 'วันที่สิ้นสุด',
  submit: 'ยืนยัน',
  no: 'ลำดับ',
  game: 'เกมส์',
  pool: 'เงินกลาง',
  type: 'ประเภท',
  number: 'จำนวน',
  betAmount: 'จำนวนเดิมพัน',
  winLose: 'ชนะแพ้',
  result: 'ผล',
  GameId: 'ระยะเวลา',
  odds: 'ปัจจัย',
  noHistory: 'ไม่พบประวัติ',
  totalBet: 'จำนวนเดิมพันรวม',
  totalWinLose: 'จำนวนชนะแพ้รวม',
  numberTran: 'เลขธุรกรรม'
}

export default statement
