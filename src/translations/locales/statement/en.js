const statement = {
  statement: 'Statements',
  statementRecord: 'STATEMENT RECORDS',
  today: 'Today',
  thisWeek: 'This Week',
  lastWeek: 'Last Week',
  thisMonth: 'This Month',
  lastMonth: 'Last Month',
  startDate: 'Start Date',
  endDate: 'End Date',
  submit: 'Submit',
  no: 'NO',
  game: 'Game',
  pool: 'Pool',
  type: 'Type',
  number: 'Number',
  betAmount: 'Bet Amount',
  winLose: 'WinLose',
  result: 'Result',
  GameId: 'GameID',
  odds: 'Odds',
  noHistory: 'No record found',
  totalBet: 'Totla Bet',
  totalWinLose: 'Total WinLose',
  numberTran: 'Transaction Number'
}

export default statement
