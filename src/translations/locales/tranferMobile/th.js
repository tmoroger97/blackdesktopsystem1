const tranfer = {
  mainBalance: 'ยอดเงินคงเหลือหลัก',
  amount: 'จำนวน',
  betlist: 'รายการการเดิมพัน',
  statement: 'ประวัติคำชี้แจง',
  referrallist: 'รายการผู้อ้างอิง',
  updatebonus: 'อัพเดทโบนัส',
  applyforbonuslist: 'สมัครรายการโบนัส',
  promotionlist: 'รายการส่งเสริมการขาย',
  autopromotion: 'ปรโมชั่นอัตโนมัติ'
}

export default tranfer
