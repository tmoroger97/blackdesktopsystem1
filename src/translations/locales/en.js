import sidebar from './sidebar/en'
import btn from './btn/en'
import barProfile from './barProfile/en'
import textfield from './textfield/en'
import alert from './alert/en'
import loading from './loading/en'
import toolbarMenu from './toolbarMenu/en'
import gameName from './gameName/en'
import footer from './footer/en'
import register from './register/en'
import promotion from './promotion/en'
import userInfo from './userInfo/en.js'
import movie from './movie/en'
import info from './info/en'
import changePassword from './changePassword/en'
import tranfer from './tranferMobile/en'
import deposit from './deposit/en'
import withdraw from './withdraw/en'
import betReport from './betReport/en'
import statement from './statement/en'
import turnOver from './turnOver/en'
import ball from './ball/en'
import contact from './contact/en'

export default {
  sidebar,
  btn,
  barProfile,
  textfield,
  alert,
  loading,
  langId: 'en',
  langName: 'English',
  languages: {
    thai: 'ไทย',
    english: 'English',
    china: '中文'
  },
  toolbarMenu,
  gameName,
  footer,
  register,
  promotion,
  userInfo,
  movie,
  info,
  changePassword,
  tranfer,
  deposit,
  withdraw,
  betReport,
  statement,
  turnOver,
  ball,
  contact
}
