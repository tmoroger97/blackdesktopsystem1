const withdraw = {
  withdraw: 'Withdraw',
  withdrawList: 'Withdraw List',
  withdrawBank: 'Withdraw To Bank',
  withdrawAccount: 'Account Name',
  withdrawNumberAccount: 'Account Number',
  withdrawAmount: 'Amount Withdraw',
  submit: 'Submit',
  reset: 'Reset',
  no: 'No',
  type: 'Type',
  time: 'Time',
  amount: 'Amount',
  status: 'Status',
  option: 'Options',
  noHistory: 'No record found',
  notSupport: 'System Withdraw Not Supported'
}

export default withdraw
