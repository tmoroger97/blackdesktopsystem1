const withdraw = {
  withdraw: 'ถอนเงิน',
  withdrawList: 'รายการถอนเงิน',
  withdrawBank: 'ถอนเงินจากธนาคาร',
  withdrawAccount: 'ชื่อบัญชี',
  withdrawNumberAccount: 'เลขที่บัญชี',
  withdrawAmount: 'จำนวนเงินที่ถอน',
  submit: 'ยืนยัน',
  reset: 'รีเซ็ต',
  no: 'ลำดับ',
  type: 'ประเภท',
  time: 'เวลา',
  amount: 'จำนวน',
  status: 'สถานะ',
  option: 'ตัวเลือกเพิ่มเติม',
  noHistory: 'ไม่พบบันทึก',
  notSupport: 'ไม่รองรับระบบถอนเงิน'
}

export default withdraw
