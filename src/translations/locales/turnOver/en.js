const turnOver = {
  turnOver: 'Checking  Turnover',
  startDate: 'Start Time',
  endDate: 'End Time',
  provider: 'Provider',
  selectAll: 'Select All',
  afb: 'AFB1188 SPORTS',
  w88: 'W88 CASINO',
  ag: 'AG CASINO',
  dg: 'DG99 CASINO',
  submit: 'Submit',
  id: 'ID',
  tableStartDate: 'Start Time',
  tableEndDate: 'End Time',
  tableProvider: 'Provider',
  tableOver: 'Turnover',
  total: 'TOTAL'
}

export default turnOver
