const turnOver = {
  turnOver: 'การตรวจสอบ  ผลประกอบการ',
  startDate: 'เวลาเริ่มต้น',
  endDate: 'เวลาสิ้นสุด',
  provider: 'ผู้ให้บริการ',
  selectAll: 'เลือกทั้งหมด',
  afb: 'AFB1188 กีฬา',
  w88: 'W88 บ่อนคาสิโน',
  ag: 'AG บ่อนคาสิโน',
  dg: 'DG99 บ่อนคาสิโน',
  submit: 'แสดง',
  id: 'ID',
  tableStartDate: 'เวลาเริ่มต้น',
  tableEndDate: 'เวลาสิ้นสุด',
  tableProvider: 'ผู้ให้บริการ',
  tableOver: 'ผลประกอบการ',
  total: 'ทั้งหมด'
}

export default turnOver
