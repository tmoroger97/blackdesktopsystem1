const ball = {
  leage: 'Leage',
  date: 'Date',
  time: 'Time',
  timeLive: 'Time Live',
  extendTime: 'Extend Time',
  home: 'Home',
  away: 'Away',
  halfTime: 'Half Time',
  result: 'Result',
  live: 'Live'
}

export default ball
