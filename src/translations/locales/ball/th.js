const ball = {
  leage: 'ชื่อลีก',
  date: 'วันที่',
  time: 'เวลา',
  timeLive: 'เวลาสด',
  extendTime: 'ต่อเวลา',
  home: 'เจ้าบ้าน',
  away: 'ทีมเยือน',
  halfTime: 'ครึ่งแรก',
  result: 'ผล',
  live: 'สด'
}

export default ball
