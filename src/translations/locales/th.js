import sidebar from './sidebar/th'
import btn from './btn/th'
import barProfile from './barProfile/th'
import textfield from './textfield/th'
import alert from './alert/th'
import loading from './loading/th'
import toolbarMenu from './toolbarMenu/th'
import gameName from './gameName/th'
import footer from './footer/th'
import register from './register/th'
import promotion from './promotion/th'
import userInfo from './userInfo/th'
import movie from './movie/th'
import info from './info/th'
import changePassword from './changePassword/th'
import tranfer from './tranferMobile/th'
import deposit from './deposit/th'
import withdraw from './withdraw/th'
import betReport from './betReport/th'
import statement from './statement/th'
import turnOver from './turnOver/th'
import ball from './ball/th'
import contact from './contact/th'

export default {
  sidebar,
  btn,
  barProfile,
  textfield,
  alert,
  loading,
  langId: 'th',
  langName: 'ไทย',
  languages: {
    thai: 'ไทย',
    english: 'English',
    china: '中文'
  },
  toolbarMenu,
  gameName,
  footer,
  register,
  promotion,
  userInfo,
  movie,
  info,
  changePassword,
  tranfer,
  deposit,
  withdraw,
  betReport,
  statement,
  turnOver,
  ball,
  contact
}
