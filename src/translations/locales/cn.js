import sidebar from './sidebar/cn'
import btn from './btn/cn'
import barProfile from './barProfile/cn'
import textfield from './textfield/cn'
import alert from './alert/cn'
import loading from './loading/cn'

export default {
  sidebar,
  btn,
  barProfile,
  textfield,
  alert,
  loading,
  langId: 'cn',
  langName: '中文',
  languages: {
    thai: 'ไทย',
    english: 'English',
    china: '中文'
  }
}
