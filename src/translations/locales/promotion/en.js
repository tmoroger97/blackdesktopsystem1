const promotion = {
  all: 'SHOW ALL',
  vip: 'VIP',
  newMember: 'NEW MEMBER',
  special: 'SPECIAL',
  sport: 'SPORTSBOOK',
  casino: 'CASINO',
  slot: 'SLOT',
  winner: 'WINNER',
  newMemberRegister: 'New Member, Get Bonus 50%',
  moreInfo: 'MORE INFO',
  join: 'JOIN NOW'
}

export default promotion
