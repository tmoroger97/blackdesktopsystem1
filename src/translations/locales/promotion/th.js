const promotion = {
  all: 'แสดงทั้งหมด',
  vip: 'VIP',
  newMember: 'สมาชิกใหม่',
  special: 'พิเศษ',
  sport: 'กีฬา',
  casino: 'คาสิโน',
  slot: 'สล็อต',
  winner: 'ผู้ชนะ',
  newMemberRegister: 'สมาชิกใหม่ รับโบนัส50%',
  moreInfo: 'ข้อมูลเพิ่มเติม',
  join: 'เข้าร่วมเดี๋ยวนี้'
}

export default promotion
