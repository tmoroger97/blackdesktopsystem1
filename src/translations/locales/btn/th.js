const btn = {
  textSignout: 'ออกจากระบบ',
  textSignin: 'เข้าสู่ระบบ',
  textSingUp: 'สมัครสมาชิก',
  pokerPlayNow: 'เล่นเดี๋ยวนี้',
  playGame: 'เล่นเกมส์',
  playDemo: 'เล่นฟรี',
  search: 'ค้นหาเกมส์ slot',
  btnSearch: 'ค้นหา',
  Credit2: 'ยอดเงินคงเหลือ'
}

export default btn
