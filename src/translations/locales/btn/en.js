const btn = {
  textSignout: 'Sign Out',
  textSignin: 'Login',
  textSingUp: 'Register',
  pokerPlayNow: 'PLAY NOW',
  playGame: 'PLAY NOW',
  playDemo: 'PLAY FREE',
  search: 'Search Game Slots',
  btnSearch: 'SEARCH',
  Credit2: 'Credit'
}

export default btn
