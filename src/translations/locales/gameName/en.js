const gameName = {
  fishing: 'FISHING',
  slot: 'SLOTS',
  casino: 'CASINO',
  sport: 'U.SPORTS',
  keno: 'KENO',
  playNow: 'PLAY NOW',
  ptSlot: 'PT SLOTS',
  mgSlot: 'MG SLOTS',
  habaSlot: 'HABA SLOTS',
  gpSlot: 'W88 SLOTS',
  jokerSlot: 'JOKER123 SLOTS',
  ppSlot: 'PPLAY SLOTS',
  playStarSlot: 'PLAYSTAR SLOTS',
  dragonSlot: 'DRAGOON SLOTS',
  fishingSlot: 'FISHING GAMES',
  hotGame: 'Hot Games'
}

export default gameName
