const gameName = {
  fishing: 'เกมส์ยิงปลา',
  slot: 'เกมส์สล็อต',
  casino: 'คาสิโนออนไลน์',
  sport: 'U.กีฬา',
  keno: 'เกมส์คีโน',
  playNow: 'เล่นเดี๋ยวนี้',
  ptSlot: 'PT สล็อต',
  mgSlot: 'MG สล็อต',
  habaSlot: 'HABA สล็อต',
  gpSlot: 'W88 สล็อต',
  jokerSlot: 'JOKER123 สล็อต',
  ppSlot: 'PPLAY สล็อต',
  playStarSlot: 'PLATSTAR สล็อต',
  dragonSlot: 'DRAGOON สล็อต',
  fishingSlot: 'เกมส์ตกปลา',
  hotGame: 'เกมยอดนิยม'
}

export default gameName
