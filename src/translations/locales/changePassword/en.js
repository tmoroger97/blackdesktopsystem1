const changePassword = {
  changePass: 'Change password',
  accountName: 'Account Name',
  currentPass: 'Current Pasword',
  newPass: 'New Password',
  confirmPass: 'Confirm Password',
  submit: 'SUBMIT'
}

export default changePassword
