const changePassword = {
  changePass: 'เปลี่ยนรหัสผ่าน',
  accountName: 'ชื่อผู้ใช้',
  currentPass: 'รหัสผ่านปัจจุบัน',
  newPass: 'รหัสผ่านใหม่',
  confirmPass: 'ยืนยันรหัสผ่าน',
  submit: 'ยืนยัน'
}

export default changePassword
