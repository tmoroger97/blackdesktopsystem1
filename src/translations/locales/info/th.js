const info = {
  account: 'บัญชี',
  refLink: 'ลิงค์อ้างอิง',
  email: 'อีเมล์',
  tel: 'โทรศัพท์',
  withdrawBankName: 'บัญชีธนาคารการถอนเงิน',
  withdrawBankNo: 'เลขบัญชีถอนเงินจากธนาคาร',
  withdrawBank: 'ถอนเงินจากธนาคาร',
  totalBonus: 'โบนัสทั้งหมด',
  lastBonus: 'โบนัสล่าสุด',
  accountBalance: 'ยอดเงินคงเหลือในบัญชี (THB)',
  refName: 'รายชื่อการอ้างอิง',
  regisForBonus: 'สมัครสำหรับรายการโบนัส',
  regisDateTime: 'เวลาที่ลงทะเบียน',
  LoginLastTime: 'เข้าระบบล่าสุด',
  tranferHeadText: 'ศูนย์โอน'
}

export default info
