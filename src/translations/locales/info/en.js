const info = {
  account: 'Account',
  refLink: 'Ref Link',
  email: 'E-mail',
  tel: 'Phone',
  withdrawBankName: 'Withdrawal Bank Account Name',
  withdrawBankNo: 'Withdrawal Bank No',
  withdrawBank: 'Withdrawal Bank',
  totalBonus: 'Total Bonus',
  lastBonus: 'Last Bonus',
  accountBalance: 'Account Balance (THB)',
  refName: 'Referral List',
  regisForBonus: 'Apply for bonus list',
  regisDateTime: 'Registration Time',
  LoginLastTime: 'Last Login',
  tranferHeadText: 'Tranfer Center'
}

export default info
