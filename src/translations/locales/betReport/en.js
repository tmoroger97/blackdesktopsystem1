const betReport = {
  betReport: 'Bet Reports',
  no: 'NO',
  game: 'game',
  pool: 'Pool',
  type: 'Type',
  number: 'Number',
  rate: 'Rate',
  discount: 'Discount',
  betAmount: 'Bet Amount',
  gameId: 'GameID',
  betTime: 'Bet Time',
  noHistory: 'No record found'
}

export default betReport
