const betReport = {
  betReport: 'รายงานการเดิมพัน',
  no: 'ลำดับ',
  game: 'เกมส์',
  pool: 'เงินกลาง',
  type: 'ประเภท',
  number: 'จำนวน',
  rate: 'ราคา',
  discount: 'ส่วนลด',
  betAmount: 'เดิมพันจำนวน',
  gameId: 'ระยะเวลา',
  betTime: 'เวลาเดิมพัน',
  noHistory: 'ไม่พบบันทึก'
}

export default betReport
