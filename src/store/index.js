import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import language from './modules/language'
import theme from './modules/theme'
import postAuth from './modules/auth/post'
import getAuth from './modules/auth/get'
import getUser from './modules/user/get'
import dialog from './modules/dialog/dialog'
import movie from './modules/movie/movie'
import listGameSlot from './modules/game/listGameSlot'
import listGameCasino from './modules/game/listGameCasino'
import postListGameSlot from './modules/game/postListGameSlot'
import postListGameCasino from './modules/game/postListGameCasino'
import statement from './modules/userInfo/statement'
import turnover from './modules/userInfo/turnover'
import bank from './modules/userInfo/bank'
import scoreball from './modules/game/scoreBall'
import postGameSlotDemo from './modules/game/postGameSlotDemo'
import getGameCasinoDemo from './modules/game/getGameCasinoDemo'
import postSearch from './modules/game/postSearch'

Vue.use(Vuex)
Vue.use(Vuex)
Vue.use(VueAxios, axios)

const store = new Vuex.Store({
  modules: {
    language,
    theme,
    postAuth,
    getAuth,
    getUser,
    dialog,
    movie,
    listGameSlot,
    listGameCasino,
    postListGameSlot,
    postListGameCasino,
    statement,
    turnover,
    bank,
    scoreball,
    postGameSlotDemo,
    getGameCasinoDemo,
    postSearch
  }
})

export default store
