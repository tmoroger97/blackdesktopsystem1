const configuregeneral = {
  /**
  * ! development
  */
  // urlService: 'http://bbw888.net:8080/webservices/',
  // urlToGameHallYouLian: 'http://gamehall.bbw888.net/slot/youLian/',
  // urlToGameHallPragmaticPlay: 'http://gamehall.bbw888.net/slot/pragmaticPlay/',
  // urlToGameHallTpg: 'http://gamehall.bbw888.net/slot/tpg/',
  // urlToGameJoker: 'http://joker.bbw888.net/game/',
  // urlToGameHallCqnine: 'http://gamehall.bbw888.net/slot/cqnine/',
  // urlToGameHallHabanero: 'http://gamehall.bbw888.net/slot/habanero/',
  // urlToGameHallDreamtech: 'http://gamehall.bbw888.net/slot/dreamtech/',
  // urlToGameBTi: 'https://stg20107.core-tech.dev/',
  // urlToGameBTi: 'https://stg20107.core-tech.dev/?operatorToken=',
  // urlToGameEvo: 'http://gamehall.bbw888.net/slot/evo/',
  // urlPgSoft: 'http://gamehall.bbw888.net/slot/pgSoft/',
  /**
  * ! production
  */
  urlService: 'https://api.empress88.com/webservices/',
  urlScoreBall: 'https://www.188stream.com/api/list?lng=en&key=352cd86958f14286986819f9666a2e5a',
  // urlToGameHallYouLian: 'http://gamehall.sportbest88.com/slot/youLian/',
  // urlToGameHallPragmaticPlay: 'http://gamehall.sportbest88.com/slot/pragmaticPlay/',
  // urlToGameHallTpg: 'http://gamehall.sportbest88.com/slot/tpg/',
  // urlToGameJoker: 'http://joker.sportbest88.com/game/',
  // urlToGameHallCqnine: 'http://gamehall.sportbest88.com/slot/cqnine/',
  // urlToGameHallHabanero: 'http://gamehall.sportbest88.com/slot/habanero/',
  // urlToGameHallDreamtech: 'http://gamehall.sportbest88.com/slot/dreamtech/',
  urlToGameBTi: 'https://prod20107.bti.bet/th/?operatorToken=',
  // urlToGameEvo: 'http://gamehall.sportbest88.com/slot/evo/',
  // urlPgSoft: 'http://gamehall.sportbest88.net/slot/pgSoft/',
  /**
 * ! general
 */
  // urlService: 'https://api.palin-2559.com/webservices/',
  keySessionStorage: '_gamehall_auth',
  siteName: 'eclub'
}

export default configuregeneral
