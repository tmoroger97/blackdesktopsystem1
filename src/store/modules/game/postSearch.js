import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import configuregeneral from '../general'
import store from '../../../store'
// import router from '../../../router'

// var notificationPostAuth = {
//   position: 'rightTop',
//   timeout: 3000,
//   titleMaxLength: 100,
//   bodyMaxLength: 200,
//   showProgressBar: false,
//   closeOnClick: false,
//   pauseOnHover: true
// }

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default {
  state: {
    modalSearch: false,
    storeDataGameSearchSlot: null
  },
  getters: {
    modalSearch: state => state.modalSearch,
    storeDataGameSearchSlot: state => state.storeDataGameSearchSlot
  },
  mutations: {
    modalSearch (state, payload) {
      state.modalSearch = payload
    },
    storeDataGameSearchSlot (state, payload) {
      state.storeDataGameSearchSlot = payload
    }
  },
  actions: {
    postStoreSlotSearch: async ({ commit, dispatch }, item) => {
      commit('preLoadingFullScreen', {
        status: true
      })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.post(
              configuregeneral.urlService + 'searchAllGameName',
              {
                search_gameName: item
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // console.log(data.data.data)
              commit('storeDataGameSearchSlot', data.data.data)
              commit('preLoadingFullScreen', {
                status: false
              })
            }
          } catch (error) {
            // // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
            if (error.response.status === 400) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 403) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 404) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 204) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 500) {
              commit('responseStoreCatch', error.response)
            }
          }
          resolve(data)
        }, 1000)
      })
    },
    postStoreSlotSearchPlayGame: async ({ commit, dispatch }, item) => {
      commit('preLoadingFullScreen', {
        status: true
      })
      console.log(item)
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.post(
              configuregeneral.urlService + item.apilink,
              {
                gameTypeID: item.gameTypeID,
                gameID: item.gameID,
                gameName: item.gameName
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // console.log(data.data.data)
              window.open(data.data.data, '_blank')
              commit('preLoadingFullScreen', {
                status: false
              })
            }
          } catch (error) {
            // // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
            if (error.response.status === 400) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 403) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 404) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 204) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 500) {
              commit('responseStoreCatch', error.response)
            }
          }
          resolve(data)
        }, 1000)
      })
    }
  }
}
