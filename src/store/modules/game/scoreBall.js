import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import configuregeneral from '../general'
import store from '../../../store'
// import router from '../../../router'

// var notificationPostAuth = {
//   position: 'rightTop',
//   timeout: 3000,
//   titleMaxLength: 100,
//   bodyMaxLength: 200,
//   showProgressBar: false,
//   closeOnClick: false,
//   pauseOnHover: true
// }

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default {
  state: {
    listTypeScoreType: [],
    listAllScore: []
  },
  getters: {
    listTypeScoreType: state => state.listTypeScoreType,
    listAllScore: state => state.listAllScore
  },
  mutations: {
    listTypeScoreType (state, payload) {
      state.listTypeScoreType = payload
    },
    listAllScore (state, payload) {
      state.listAllScore = payload
    }
  },
  actions: {
    getStoreListScoreType: async ({ commit, dispatch }) => {
      commit('preLoadingFullScreen', {
        status: true
      })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.get(
              configuregeneral.urlService + 'GetTypeSportAPI',
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // console.log(data.data.data)
              commit('listTypeScoreType', data.data.data)
              // window.open(data.data.data, '_blank')
              // commit('preLoadingFullScreen', {
              //   status: false
              // })
              var toCall = {
                sportName: data.data.data[0].sportName
              }
              store.dispatch('postStoreListAllScore', toCall)
            }
          } catch (error) {
            console.log(error)
            commit('preLoadingFullScreen', {
              status: false
            })
            if (error.response.status === 400) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 403) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 404) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 204) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 500) {
              var errorText = {
                status: 500,
                data: '',
                statusText: 'Maintenance . . .'
              }
              commit('responseStoreCatch', errorText)
              // commit('responseStoreCatch', error.response)
            }
          }
          resolve(data)
        }, 2000)
      })
    },
    postStoreListAllScore: async ({ commit, dispatch }, item) => {
      commit('preLoadingFullScreen', {
        status: true
      })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.post(
              configuregeneral.urlService + 'GetLeagueBySportAPI',
              item,
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // console.log(data.data.data)
              commit('listAllScore', data.data.data)
              // window.open(data.data.data, '_blank')
              commit('preLoadingFullScreen', {
                status: false
              })
            }
          } catch (error) {
            // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
            if (error.response.status === 400) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 403) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 404) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 204) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 500) {
              commit('responseStoreCatch', error.response)
            }
          }
          resolve(data)
        }, 2000)
      })
    }
  }
}
