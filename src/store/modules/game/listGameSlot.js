import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import configuregeneral from '../general'
// import store from '../../../store'
// import router from '../../../router'

// var notificationPostAuth = {
//   position: 'rightTop',
//   timeout: 3000,
//   titleMaxLength: 100,
//   bodyMaxLength: 200,
//   showProgressBar: false,
//   closeOnClick: false,
//   pauseOnHover: true
// }

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default {
  state: {
    getListGameSlot: null,
    getGameSlot: null,
    getGameSlotFishing: null
  },
  getters: {
    getListGameSlot: state => state.getListGameSlot,
    getGameSlot: state => state.getGameSlot,
    getGameSlotFishing: state => state.getGameSlotFishing
  },
  mutations: {
    getListGameSlot (state, payload) {
      state.getListGameSlot = payload
    },
    getGameSlot (state, payload) {
      state.getGameSlot = payload
    },
    getGameSlotFishing (state, payload) {
      state.getGameSlotFishing = payload
    }
  },
  actions: {
    getStoreListGameSlot: async ({ commit, dispatch }) => {
      // commit('preLoadingFullScreen', {
      //   status: true
      // })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.get(
              configuregeneral.urlService + 'HomeListGame',
              {
                headers: {
                  'Content-Type': 'application/json'
                  // eslint-disable-next-line
                  // 'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // console.log(data.data)
              commit('getListGameSlot', data.data)
              // window.open(data.data.data, '_blank')
              // commit('preLoadingFullScreen', {
              //   status: false
              // })
            }
          } catch (error) {
            // // console.log(error.response)
            // commit('preLoadingFullScreen', {
            //   status: false
            // })
          }
          resolve(data)
        }, 0)
      })
    },
    getStoreSlotGame: async ({ commit, dispatch }, item) => {
      commit('preLoadingFullScreen', {
        status: true
      })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.get(
              configuregeneral.urlService + item,
              {
                headers: {
                  'Content-Type': 'application/json'
                  // eslint-disable-next-line
                  // 'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // console.log(data.data.data)
              commit('getGameSlot', data.data.data)
              // router.push('/desktop/Slot/listSlotGame/' + item.fullname)
              // window.open(data.data.data, '_blank')
              commit('preLoadingFullScreen', {
                status: false
              })
            }
          } catch (error) {
            // // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
          }
          resolve(data)
        }, 1000)
      })
    },
    getStoreSlotGameFishing: async ({ commit, dispatch }, item) => {
      commit('preLoadingFullScreen', {
        status: true
      })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.get(
              configuregeneral.urlService + 'GetFishingGame',
              {
                headers: {
                  'Content-Type': 'application/json'
                  // eslint-disable-next-line
                  // 'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // // console.log(data.data.data)
              commit('getGameSlotFishing', data.data.data)
              // router.push('/desktop/Slot/listSlotGame/' + item.fullname)
              // window.open(data.data.data, '_blank')
              commit('preLoadingFullScreen', {
                status: false
              })
            }
          } catch (error) {
            // // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
          }
          resolve(data)
        }, 1000)
      })
    }
  }
}
