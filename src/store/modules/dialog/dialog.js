import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default {
  state: {
    dialogLoginDesktop: false
  },
  getters: {
    dialogLoginDesktop: state => state.dialogLoginDesktop
  },
  mutations: {
    setAlertDialogLoginDesktop (state, payload) {
      state.dialogLoginDesktop = payload
    }
  },
  actions: {
  }
}
