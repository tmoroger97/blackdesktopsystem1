import Vue from 'vue'
import VueLocalStorage from 'vue-localstorage'
import Languages from '../../translations'
// import store from '../../store'
// import router from '../../router'

Vue.use(VueLocalStorage)
const supportedLanguages = Object.getOwnPropertyNames(Languages)

export default {
  namespaced: true,
  state: {
    // dataGlobalLanguage: 1,
    language: Vue.localStorage.get('language')
  },
  mutations: {
    SET_LANGUAGE (state, lang) {
      Vue.localStorage.set('language', lang)
      state.language = lang
    }
  },
  actions: {
    setLanguage ({ commit, state }, languages) {
      if (typeof languages === 'string') {
        commit('SET_LANGUAGE', languages)
      } else {
        const language = supportedLanguages.find(sl =>
          languages.find(l => (l.split(new RegExp(sl, 'gi')).length - 1 > 0 ? sl : null)))
        commit('SET_LANGUAGE', language)
      }
      // if (state.language === 'th') {
      //   state.dataGlobalLanguage = 1
      // } else if (state.language === 'en') {
      //   state.dataGlobalLanguage = 2
      // } else if (state.language === 'cn') {
      //   state.dataGlobalLanguage = 3
      // }
      // // // console.log(router)
      // Vue.localStorage.set('globalLanguageStorage', state.dataGlobalLanguage)
      // store.commit('globalLanguage', state.dataGlobalLanguage)
      // if (router.currentRoute.name === 'home') {
      //   store.dispatch('getDataStoreAllProducts')
      // } else if (router.currentRoute.name === 'productsDetail') {
      //   store.dispatch('getDataStoreProductsDetail', router.currentRoute.params.productcorlor_uuid)
      // } else if (router.currentRoute.name === 'category') {
      //   var dataProductsInSubOptionColor = {
      //     cat_uuid: router.currentRoute.query.cat_uuid,
      //     cat_sub_uuid: JSON.parse(router.currentRoute.query.cat_sub_uuid),
      //     option_uuid: JSON.parse(router.currentRoute.query.option_uuid),
      //     color_uuid: JSON.parse(router.currentRoute.query.color_uuid)
      //   }
      //   store.dispatch('getDataStoreCategory')
      //   store.dispatch('getDataStoreProductsInSubOptionColor', dataProductsInSubOptionColor)
      // } else if (router.currentRoute.name === 'aboutus') {
      //   store.dispatch('getDataStoreAboutus')
      // } else if (router.currentRoute.name === 'checkStatus') {
      //   store.commit('languageThaipost', state.language.toUpperCase())
      // }
    }
  }
}
