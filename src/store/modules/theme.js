import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueLocalStorage from 'vue-localstorage'
import theme from '../../theme'

Vue.use(Vuex)
Vue.use(VueAxios, axios)
Vue.use(VueLocalStorage)

export default {
  state: {
    setTheme: JSON.parse(Vue.localStorage.get('setDataTheme'))
  },
  getters: {
    setTheme: state => state.setTheme
  },
  mutations: {
    setTheme (state, lang) {
      state.setTheme = lang
    }
  },
  actions: {
    setStoreDataTheme ({ commit, state }, item) {
      if (item === 'red') {
        Vue.localStorage.set('setDataTheme', JSON.stringify(theme.red))
        commit('setTheme', theme.red)
      } else if (item === 'pink') {
        Vue.localStorage.set('setDataTheme', JSON.stringify(theme.pink))
        commit('setTheme', theme.pink)
      } else if (item === 'purple') {
        Vue.localStorage.set('setDataTheme', JSON.stringify(theme.purple))
        commit('setTheme', theme.purple)
      } else if (item === 'deeppurple') {
        Vue.localStorage.set('setDataTheme', JSON.stringify(theme.deeppurple))
        commit('setTheme', theme.deeppurple)
      } else if (item === 'indigo') {
        Vue.localStorage.set('setDataTheme', JSON.stringify(theme.indigo))
        commit('setTheme', theme.indigo)
      } else if (item === 'blue') {
        Vue.localStorage.set('setDataTheme', JSON.stringify(theme.blue))
        commit('setTheme', theme.blue)
      } else if (item === 'brown') {
        Vue.localStorage.set('setDataTheme', JSON.stringify(theme.brown))
        commit('setTheme', theme.brown)
      } else if (item === 'bluegrey') {
        Vue.localStorage.set('setDataTheme', JSON.stringify(theme.bluegrey))
        commit('setTheme', theme.bluegrey)
      } else if (item === 'teal') {
        Vue.localStorage.set('setDataTheme', JSON.stringify(theme.teal))
        commit('setTheme', theme.teal)
      } else if (item === 'green') {
        Vue.localStorage.set('setDataTheme', JSON.stringify(theme.green))
        commit('setTheme', theme.green)
      } else if (item === 'lightgreen') {
        Vue.localStorage.set('setDataTheme', JSON.stringify(theme.lightgreen))
        commit('setTheme', theme.lightgreen)
      } else if (item === 'orange') {
        Vue.localStorage.set('setDataTheme', JSON.stringify(theme.orange))
        commit('setTheme', theme.orange)
      } else {
        commit('setTheme', '')
      }
    }
  }
}
