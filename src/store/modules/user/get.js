import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import configuregeneral from '../general'
import store from '../../../store'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default {
  state: {
    getMsgStoreUser: null,
    getDataUserInfo: null
  },
  getters: {
    getMsgStoreUser: state => state.getMsgStoreUser,
    getDataUserInfo: state => state.getDataUserInfo
  },
  mutations: {
    getMsgStoreUser (state, payload) {
      state.getMsgStoreUser = payload
    },
    getDataUserInfo (state, payload) {
      state.getDataUserInfo = payload
    }
  },
  actions: {
    getDataStoreUser: async ({ commit, dispatch }) => {
      // commit('preLoadingFullScreen', {
      //   status: true
      // })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.get(
              configuregeneral.urlService + 'getDataUserLogin',
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // console.log(data.data)
              commit('getMsgStoreUser', data.data.data)
              // commit('preLoadingFullScreen', {
              //   status: false
              // })
            }
          } catch (error) {
            // // console.log(error.response)
            // commit('preLoadingFullScreen', {
            //   status: false
            // })
            if (error.response.status === 400) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 403) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 404) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 204) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 500) {
              commit('responseStoreCatch', error.response)
            }
          }
          resolve(data)
        }, 0)
      })
    },
    getStoreUserInfo: async ({ commit, dispatch }) => {
      // commit('preLoadingFullScreen', {
      //   status: true
      // })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.get(
              configuregeneral.urlService + 'getDataUserLogin',
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // // console.log(data.data.data)
              commit('getDataUserInfo', data.data.data)
              // commit('preLoadingFullScreen', {
              //   status: false
              // })
            }
          } catch (error) {
            // // console.log(error.response)
            // commit('preLoadingFullScreen', {
            //   status: false
            // })
            if (error.response.status === 400) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 403) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 404) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 204) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 500) {
              commit('responseStoreCatch', error.response)
            }
          }
          resolve(data)
        }, 1000)
      })
    }
  }
}
