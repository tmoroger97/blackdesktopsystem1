import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import configuregeneral from '../general'
import store from '../../../store'
// import router from '../../../router'

var notificationPostAuth = {
  position: 'rightBottom',
  timeout: 3000,
  titleMaxLength: 100,
  bodyMaxLength: 200,
  showProgressBar: false,
  closeOnClick: false,
  pauseOnHover: true
}

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default {
  state: {
    listBank: null,
    historyDeposit: null
  },
  getters: {
    listBank: state => state.listBank,
    historyDeposit: state => state.historyDeposit
  },
  mutations: {
    listBank (state, payload) {
      state.listBank = payload
    },
    historyDeposit (state, payload) {
      state.historyDeposit = payload
    }
  },
  actions: {
    getDataStoreListBank: async ({ commit, dispatch }, items) => {
      // commit('preLoadingFullScreen', {
      //   status: true
      // })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.post(
              configuregeneral.urlService + 'GetBankTransferCallback/',
              {},
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // console.log(data.data)
              commit('listBank', data.data.data)
              // commit('preLoadingFullScreen', {
              //   status: false
              // })
            }
          } catch (error) {
            // console.log(error.response)
            // commit('preLoadingFullScreen', {
            //   status: false
            // })
            if (error.response.status === 400) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 403) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 404) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 204) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 500) {
              commit('responseStoreCatch', error.response)
            }
          }
          resolve(data)
        }, 1000)
      })
    },
    getDataStoreHistoryDeposit: async ({ commit, dispatch }, items) => {
      // commit('preLoadingFullScreen', {
      //   status: true
      // })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.post(
              configuregeneral.urlService + 'GetHistoryCallback/',
              {},
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // console.log(data.data.data)
              commit('historyDeposit', data.data.data)
              // commit('preLoadingFullScreen', {
              //   status: false
              // })
            }
          } catch (error) {
            // console.log(error.response)
            // commit('preLoadingFullScreen', {
            //   status: false
            // })
            if (error.response.status === 400) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 403) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 404) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 204) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 500) {
              commit('responseStoreCatch', error.response)
            }
          }
          resolve(data)
        }, 1000)
      })
    },
    // ---------------------------- withdraw --------------------------------------------------------
    postDataStoreWithdraw: async ({ commit, dispatch }, items) => {
      // console.log(items)
      commit('preLoadingFullScreen', {
        status: true
      })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.post(
              configuregeneral.urlService + 'CallbackWallet/',
              items,
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // console.log(data)
              // commit('historyDeposit', data.data.data)
              commit('preLoadingFullScreen', {
                status: false
              })
              Vue.$snotify.success(
                data.data.data,
                notificationPostAuth
              )
            }
          } catch (error) {
            // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
            if (error.response.status === 400) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 403) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 404) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 204) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 500) {
              commit('responseStoreCatch', error.response)
            }
            Vue.$snotify.error(
              '(' +
              error.response.status +
              ')' +
              ' ' +
              error.response.statusText,
              error.response.data.message,
              notificationPostAuth
            )
          }
          resolve(data)
        }, 1000)
      })
    }
  }
}
