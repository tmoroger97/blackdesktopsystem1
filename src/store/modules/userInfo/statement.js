import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import configuregeneral from '../general'
import store from '../../../store'
// import router from '../../../router'

// var notificationPostAuth = {
//   position: 'rightTop',
//   timeout: 3000,
//   titleMaxLength: 100,
//   bodyMaxLength: 200,
//   showProgressBar: false,
//   closeOnClick: false,
//   pauseOnHover: true
// }

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default {
  state: {
    getListGameByUser: null,
    getStatementByUser: [],
    statusGetStatementByUser: false
  },
  getters: {
    getListGameByUser: state => state.getListGameByUser,
    getStatementByUser: state => state.getStatementByUser,
    statusGetStatementByUser: state => state.statusGetStatementByUser
  },
  mutations: {
    getListGameByUser (state, payload) {
      state.getListGameByUser = payload
    },
    getStatementByUser (state, payload) {
      state.getStatementByUser = payload
    },
    statusGetStatementByUser (state, payload) {
      state.statusGetStatementByUser = payload
    }
  },
  actions: {
    storeListGameByUser: async ({ commit, dispatch }) => {
      // commit('preLoadingFullScreen', {
      //   status: true
      // })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.get(
              configuregeneral.urlService + 'GetListGameAPI',
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // // console.log(data.data.data)
              // var dataGameTrue = []
              // for (var i = 0; i < data.data.data.length; i++) {
              //   if (data.data.data[i].show_web === true) {
              //     dataGameTrue.push(data.data.data[i])
              //   }
              // }
              commit('getListGameByUser', data.data.data)
              // window.open(data.data.data, '_blank')
              // commit('preLoadingFullScreen', {
              //   status: false
              // })
            }
          } catch (error) {
            // // console.log(error.response)
            // commit('preLoadingFullScreen', {
            //   status: false
            // })
            if (error.response.status === 400) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 403) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 404) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 204) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 500) {
              commit('responseStoreCatch', error.response)
            }
          }
          resolve(data)
        }, 1000)
      })
    },
    postStoreListStatementByUser: async ({ commit, dispatch }, item) => {
      commit('statusGetStatementByUser', true)
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.post(
              configuregeneral.urlService + 'statementByuser',
              item,
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // // console.log(data.data.data)
              commit('getStatementByUser', data.data.data)
              // window.open(data.data.data, '_blank')
              commit('statusGetStatementByUser', false)
            }
          } catch (error) {
            // // console.log(error.response)
            commit('statusGetStatementByUser', false)
            if (error.response.status === 400) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 403) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 404) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 204) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 500) {
              commit('responseStoreCatch', error.response)
            }
          }
          resolve(data)
        }, 1000)
      })
    }
  }
}
