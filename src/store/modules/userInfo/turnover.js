import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import configuregeneral from '../general'
import store from '../../../store'
// import router from '../../../router'

// var notificationPostAuth = {
//   position: 'rightTop',
//   timeout: 3000,
//   titleMaxLength: 100,
//   bodyMaxLength: 200,
//   showProgressBar: false,
//   closeOnClick: false,
//   pauseOnHover: true
// }

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default {
  state: {
    getListGameTurnOver: null,
    getTurnOver: null,
    statusGetTurnOver: false
  },
  getters: {
    getListGameTurnOver: state => state.getListGameTurnOver,
    getTurnOver: state => state.getTurnOver,
    statusGetTurnOver: state => state.statusGetTurnOver
  },
  mutations: {
    getListGameTurnOver (state, payload) {
      state.getListGameTurnOver = payload
    },
    getTurnOver (state, payload) {
      state.getTurnOver = payload
    },
    statusGetTurnOver (state, payload) {
      state.statusGetTurnOver = payload
    }
  },
  actions: {
    getStoreListGameTurnOverByUser: async ({ commit, dispatch }) => {
      // commit('preLoadingFullScreen', {
      //   status: true
      // })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.get(
              configuregeneral.urlService + 'GetListGameAPI',
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // // // console.log(data.data.data)
              var statusCheck = []
              for (var i = 0; i < data.data.data.length; i++) {
                var newData = {
                  provider_id: data.data.data[i].provider_id,
                  provider_name: data.data.data[i].provider_name,
                  status: false
                }
                statusCheck.push(newData)
              }
              // // console.log(statusCheck)
              commit('getListGameTurnOver', statusCheck)
              // commit('preLoadingFullScreen', {
              //   status: false
              // })
            }
          } catch (error) {
            // // console.log(error.response)
            // commit('preLoadingFullScreen', {
            //   status: false
            // })
            if (error.response.status === 400) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 403) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 404) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 204) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 500) {
              commit('responseStoreCatch', error.response)
            }
          }
          resolve(data)
        }, 1000)
      })
    },
    getStoreListTurnOverByUser: async ({ commit, dispatch }, item) => {
      commit('statusGetTurnOver', true)
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.post(
              configuregeneral.urlService + 'turnOverByuser',
              item,
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // // console.log(data.data.data)
              commit('getTurnOver', data.data.data)
              // window.open(data.data.data, '_blank')
              commit('statusGetTurnOver', false)
            }
          } catch (error) {
            // // console.log(error.response)
            commit('statusGetTurnOver', false)
            if (error.response.status === 400) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 403) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 404) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 204) {
              commit('responseStoreCatch', error.response)
            } else if (error.response.status === 500) {
              commit('responseStoreCatch', error.response)
            }
          }
          resolve(data)
        }, 1000)
      })
    }
  }
}
