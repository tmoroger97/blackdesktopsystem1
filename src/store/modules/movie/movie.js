import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
// import configuregeneral from '../general'
import store from '../../../store'
// import router from '../../../router'

var notificationPostAuth = {
  position: 'rightTop',
  timeout: 3000,
  titleMaxLength: 100,
  bodyMaxLength: 200,
  showProgressBar: false,
  closeOnClick: false,
  pauseOnHover: true
}

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default {
  state: {
    movieListCategory: '',
    movieListType: '',
    movieList: '',
    movieDetail: '',
    dataMovie: '',
    statusMovie: false
  },
  getters: {
    movieListCategory: state => state.movieListCategory,
    movieListType: state => state.movieListType,
    movieList: state => state.movieList,
    movieDetail: state => state.movieDetail,
    dataMovie: state => state.dataMovie,
    statusMovie: state => state.statusMovie
  },
  mutations: {
    movieListCategory (state, payload) {
      state.movieListCategory = payload
    },
    movieListType (state, payload) {
      state.movieListType = payload
    },
    movieList (state, payload) {
      state.movieList = payload
    },
    movieDetail (state, payload) {
      state.movieDetail = payload
    },
    clearMovie (state, payload) {
      state.movieDetail = payload
    },
    setDataMovie (state, payload) {
      state.dataMovie = payload
    },
    statusMovie (state, payload) {
      state.statusMovie = payload
    }
  },
  actions: {
    getDataMovieCategory: async ({ commit, dispatch }, item) => {
      return new Promise(function (resolve, reject) {
        commit('preLoadingFullScreen', {
          status: true
        })
        setTimeout(async () => {
          try {
            const data = await axios.get(
              'https://asia-east2-alphamovie4k.cloudfunctions.net/GetAllMovieCategory'
            )
            if (data) {
              // // console.log(data.data)
              commit('statusMovie', true)
              commit('movieListCategory', data.data)
              commit('preLoadingFullScreen', {
                status: false
              })
              store.dispatch('getDataMovieType')
              store.dispatch('getDataMovieListByCategory', data.data[0])
            }
          } catch (error) {
            // // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
            Vue.$snotify.error(error.response.data.result +
            error.response.status,
            error.response.data.message,
            notificationPostAuth)
          }
          resolve()
        }, 1000)
      })
    },
    getDataMovieType: async ({ commit, dispatch }, item) => {
      return new Promise(function (resolve, reject) {
        commit('preLoadingFullScreen', {
          status: true
        })
        setTimeout(async () => {
          try {
            const data = await axios.get(
              'https://asia-east2-alphamovie4k.cloudfunctions.net/GetAllMovieType'
            )
            if (data) {
              // // console.log(data.data)
              commit('movieListType', data.data)
              commit('preLoadingFullScreen', {
                status: false
              })
              // store.dispatch('getDatMovieListByCategory', data.data[0])
            }
          } catch (error) {
            // // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
            Vue.$snotify.error(error.response.data.result +
            error.response.status,
            error.response.data.message,
            notificationPostAuth)
          }
          resolve()
        }, 1000)
      })
    },
    // ----------------------------------------    list movie -------------------------------------------------------------------------
    getDataMovieListByCategory: async ({ commit, dispatch }, item) => {
      return new Promise(function (resolve, reject) {
        commit('preLoadingFullScreen', {
          status: true
        })
        setTimeout(async () => {
          try {
            const data = await axios.get(
              'https://asia-east2-alphamovie4k.cloudfunctions.net/GetMovieList?movie_cat=' + item.movie_category_id
            )
            if (data) {
              // // console.log(data.data)
              commit('movieList', data.data)
              commit('preLoadingFullScreen', {
                status: false
              })
            }
          } catch (error) {
            // // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
            Vue.$snotify.error(error.response.data.result +
            error.response.status,
            error.response.data.message,
            notificationPostAuth)
          }
          resolve()
        }, 1000)
      })
    },
    getDataMovieListByTypeMovie: async ({ commit, dispatch }, item) => {
      return new Promise(function (resolve, reject) {
        commit('preLoadingFullScreen', {
          status: true
        })
        setTimeout(async () => {
          try {
            const data = await axios.get(
              'https://asia-east2-alphamovie4k.cloudfunctions.net/GetMovieList?movie_type=' + item.movie_type
            )
            if (data) {
              // // console.log(data.data)
              commit('movieList', data.data)
              commit('preLoadingFullScreen', {
                status: false
              })
            }
          } catch (error) {
            // // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
            Vue.$snotify.error(error.response.data.result +
            error.response.status,
            error.response.data.message,
            notificationPostAuth)
          }
          resolve()
        }, 1000)
      })
    },
    // -------------------------- load more -----------------------------------------
    getDataMovieListLoadMore: async ({ commit, dispatch }, item) => {
      return new Promise(function (resolve, reject) {
        commit('preLoadingFullScreen', {
          status: true
        })
        setTimeout(async () => {
          try {
            const data = await axios.get(
              'https://asia-east2-alphamovie4k.cloudfunctions.net/GetMovieList?movie_cat=' + item.movie_cat + '&limit=' + item.limit + '&offset=' + item.offset
            )
            if (data) {
              // // // console.log(data.data)
              commit('movieList', data.data)
              commit('preLoadingFullScreen', {
                status: false
              })
            }
          } catch (error) {
            // // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
            Vue.$snotify.error(error.response.data.result +
            error.response.status,
            error.response.data.message,
            notificationPostAuth)
          }
          resolve()
        }, 1000)
      })
    },
    // ----------------------------- detail movie-------------------------------------------
    getDataMovieDetail: async ({ commit, dispatch }, item) => {
      return new Promise(function (resolve, reject) {
        var movieUrl = item.movieapi_link
        if (item.movieapi_link === 'no') {
          movieUrl = item.movieapi_link_sub
        }
        commit('preLoadingFullScreen', {
          status: true
        })
        setTimeout(async () => {
          try {
            var data = await axios.get(
              'https://asia-east2-alphamovie4k.cloudfunctions.net/GetPlayMovieUrl?movieapi_link=' + movieUrl + '&key=test'
            )
            if (data) {
              // // // console.log(data.data)
              commit('movieDetail', data.data)
              // var test = router.resolve({
              //   name: 'pageMovie',
              //   params: {
              //     items: data.data
              //   }
              // })
              // window.open(test.href, '_blank')
              // window.open('/Movie', '_blank')
              commit('preLoadingFullScreen', {
                status: false
              })
            }
          } catch (error) {
            // // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
            Vue.$snotify.error(error.response.data.result +
            error.response.status,
            error.response.data.message,
            notificationPostAuth)
          }
          resolve(data)
        }, 0)
      })
    }
  }
}
