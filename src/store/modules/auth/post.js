import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import configuregeneral from '../general'
import store from '../../../store'
import router from '../../../router'

var notificationPostAuth = {
  position: 'rightBottom',
  timeout: 3000,
  titleMaxLength: 100,
  bodyMaxLength: 200,
  showProgressBar: false,
  closeOnClick: false,
  pauseOnHover: true
}

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default {
  state: {
    preLoadingFullScreen: false,
    // dataAccessToken: true,
    // dataAccessToken: '5469337dae9f0f322b95336a354592a4c22974fb94be34613913636402b0b5da',
    // dataAccessToken: '5b209995746c6a6de62029cbc4c30c8cab2e29b70b3e458cbdb54f179fee256d',
    dataAccessToken: window.sessionStorage.getItem(
      configuregeneral.keySessionStorage +
      '_token'
    ),
    responseStoreCatch: null
  },
  getters: {
    preLoadingFullScreen: state => state.preLoadingFullScreen,
    dataAccessToken: state => state.dataAccessToken,
    responseStoreCatch: state => state.responseStoreCatch
  },
  mutations: {
    preLoadingFullScreen (state, payload) {
      state.preLoadingFullScreen = payload
    },
    dataAccessToken (state, payload) {
      state.dataAccessToken = payload
    },
    // setToken (state, payload) {
    //   state.dataAccessToken = payload
    // },
    responseStoreCatch (state, payload) {
      state.responseStoreCatch = payload
    }
  },
  actions: {
    postDataStoreSignin: async ({ commit, dispatch }, item) => {
      // // // console.log(item)
      return new Promise(function (resolve, reject) {
        commit('preLoadingFullScreen', {
          status: true
        })
        setTimeout(async () => {
          try {
            const data = await axios.post(
              configuregeneral.urlService + 'auth/login/user/',
              {
                username: item.username,
                password: item.password
              }
            )
            if (data) {
              // // console.log(data)
              if (data.data.response === true) {
                window.sessionStorage.setItem(
                  configuregeneral.keySessionStorage +
                  '_token', data.data.access_token
                )
                commit('dataAccessToken', data.data.access_token)
                store.dispatch('getDataStoreUser')
                // router.push('/dashboard/', () => {})
                router.push('/mainpage', () => {})
              }
              commit('preLoadingFullScreen', {
                status: false
              })
            }
            commit('setAlertDialogLoginDesktop', false)
          } catch (error) {
            // // console.log(error.response)
            // // console.log(error)
            commit('preLoadingFullScreen', {
              status: false
            })
            Vue.$snotify.error(
              '(' +
              error.response.status +
              ')' +
              ' ' +
              error.response.statusText,
              error.response.data.message,
              notificationPostAuth
            )
          }
          resolve()
        }, 0)
      })
    },
    postDataStoreSignOut: async ({ commit, dispatch }, item) => {
      window.sessionStorage.removeItem(
        configuregeneral.keySessionStorage +
        '_token'
      )
      commit('dataAccessToken', null)
      router.push('/mainpage', () => {})
    },
    // ----------------------------------------------------------------------------------------------
    postDataStoreSigninMobile: async ({ commit, dispatch }, item) => {
      // // console.log(item)
      return new Promise(function (resolve, reject) {
        commit('preLoadingFullScreen', {
          status: true
        })
        setTimeout(async () => {
          try {
            const data = await axios.post(
              configuregeneral.urlService + 'auth/login/user/',
              {
                username: item.username,
                password: item.password
              }
            )
            if (data) {
              // // console.log(data)
              if (data.data.response === true) {
                window.sessionStorage.setItem(
                  configuregeneral.keySessionStorage +
                  '_token', data.data.access_token
                )
                commit('dataAccessToken', data.data.access_token)
                store.dispatch('getDataStoreUser')
                // // console.log('mobile')
                router.push('/mobile/', () => {})
              }
              commit('preLoadingFullScreen', {
                status: false
              })
            }
          } catch (error) {
            // // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
            Vue.$snotify.error(
              '(' +
              error.response.status +
              ')' +
              ' ' +
              error.response.statusText,
              error.response.data.message,
              notificationPostAuth
            )
          }
          resolve()
        }, 0)
      })
    },
    postDataStoreSignOutMobile: async ({ commit, dispatch }, item) => {
      window.sessionStorage.removeItem(
        configuregeneral.keySessionStorage +
        '_token'
      )
      commit('dataAccessToken', null)
      router.push('/mobile', () => {})
    },
    // ------------------------- change pass ---------------------------------------------------------------
    postDataChangePassword: async ({ commit, dispatch }, item) => {
      // commit('preLoadingFullScreen', {
      //   status: true
      // })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.post(
              configuregeneral.urlService + 'change_password',
              {
                password: item.password,
                newpassword: item.newpassword
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data.data.response === true) {
              // // console.log(data)
              commit('preLoadingFullScreen', {
                status: false
              })
              Vue.$snotify.success('Change Password Success', notificationPostAuth)
              store.dispatch('postDataStoreSignOut')
            } else {
              commit('preLoadingFullScreen', {
                status: false
              })
              Vue.$snotify.error('Current Password Incorrect', notificationPostAuth)
            }
          } catch (error) {
            // // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
            Vue.$snotify.error('Current Password Incorrect', notificationPostAuth)
          }
          resolve(data)
        }, 1000)
      })
    },
    postDataChangePasswordMobile: async ({ commit, dispatch }, item) => {
      commit('preLoadingFullScreen', {
        status: true
      })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.post(
              configuregeneral.urlService + 'change_password',
              {
                password: item.password,
                newpassword: item.newpassword
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data.data.response === true) {
              // // console.log(data)
              commit('preLoadingFullScreen', {
                status: false
              })
              Vue.$snotify.success('Change Password Success', notificationPostAuth)
              store.dispatch('postDataStoreSignOutMobile')
            } else {
              commit('preLoadingFullScreen', {
                status: false
              })
              Vue.$snotify.error('Current Password Incorrect', notificationPostAuth)
            }
          } catch (error) {
            // // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
            Vue.$snotify.error('Current Password Incorrect', notificationPostAuth)
          }
          resolve(data)
        }, 1000)
      })
    }
  }
}
