import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import configuregeneral from '../general'
import store from '../../../store'
// import router from '../../../router'

// var notificationPostAuth = {
//   position: 'rightBottom',
//   timeout: 3000,
//   titleMaxLength: 100,
//   bodyMaxLength: 200,
//   showProgressBar: false,
//   closeOnClick: false,
//   pauseOnHover: true
// }

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default {
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
    getDataStoreAuthAuto: async ({ commit, dispatch }, items) => {
      commit('preLoadingFullScreen', {
        status: true
      })
      return new Promise(function (resolve, reject) {
        setTimeout(async () => {
          try {
            var data = await axios.get(
              configuregeneral.urlService + 'auto_login_user/' + items,
              {
                headers: {
                  'Content-Type': 'application/json',
                  // eslint-disable-next-line
                  'authorization': store.state.postAuth.dataAccessToken
                }
              }
            )
            if (data) {
              // console.log(data)
              window.sessionStorage.setItem(
                configuregeneral.keySessionStorage +
                '_token', data.data.access_token
              )
              commit('dataAccessToken', data.data.access_token)
              store.dispatch('getDataStoreUser')
              // if (items.pvId === '2') {
              //   router.push('/desktop/Slot/listSlotGame/getJokerGamesNew/JokerGaming/Joker%20Gaming')
              // } else if (items.pvId === '16') {
              //   router.push('/desktop/Slot/listSlotGame/getGameDTNew/DreamTech/DreamTech')
              // }
              commit('preLoadingFullScreen', {
                status: false
              })
            }
          } catch (error) {
            // console.log(error.response)
            commit('preLoadingFullScreen', {
              status: false
            })
          }
          resolve(data)
        }, 1000)
      })
    }
  }
}
