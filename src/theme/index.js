import red from './color/red'
import pink from './color/pink'
import purple from './color/purple'
import deeppurple from './color/deeppurple'
import indigo from './color/indigo'
import blue from './color/blue'
import brown from './color/brown'
import bluegrey from './color/bluegrey'
import teal from './color/teal'
import green from './color/green'
import lightgreen from './color/lightgreen'
import orange from './color/orange'

export default {
  red,
  pink,
  purple,
  deeppurple,
  indigo,
  blue,
  brown,
  bluegrey,
  teal,
  green,
  lightgreen,
  orange
}
