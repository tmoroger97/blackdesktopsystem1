const lightgreen = {
  nameColor: 'lightgreen',
  sidebar: '#558B2F',
  barProfile: '#9CCC65'
}

export default lightgreen
