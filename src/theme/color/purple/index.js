const purple = {
  nameColor: 'purple',
  sidebar: '#6A1B9A',
  barProfile: '#EA80FC'
}

export default purple
