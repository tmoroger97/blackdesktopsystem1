const pink = {
  nameColor: 'pink',
  sidebar: '#AD1457',
  barProfile: '#FF80AB'
}

export default pink
