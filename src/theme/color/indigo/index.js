const indigo = {
  nameColor: 'indigo',
  sidebar: '#283593',
  barProfile: '#8C9EFF'
}

export default indigo
