const blue = {
  nameColor: 'blue',
  sidebar: '#1565C0',
  barProfile: '#64B5F6'
}

export default blue
