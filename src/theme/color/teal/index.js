const teal = {
  nameColor: 'teal',
  sidebar: '#00695C',
  barProfile: '#80CBC4'
}

export default teal
