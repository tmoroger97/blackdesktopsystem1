const deeppurple = {
  nameColor: 'deeppurple',
  sidebar: '#4527A0',
  barProfile: '#B388FF'
}

export default deeppurple
