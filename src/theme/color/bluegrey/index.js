const bluegrey = {
  nameColor: 'bluegrey',
  sidebar: '#607D8B',
  barProfile: '#90A4AE'
}

export default bluegrey
