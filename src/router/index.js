import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import { i18n } from '../plugins/i18n'
import store from '../store'
import configuregeneral from '../store/modules/general'

import dashboard from '../views/dashboard/Dashboard.vue'
import login from '../views/login/Login.vue'

import register from '../components/useAll/register.vue'

// ------------------------------------ desktop -------------------
import homePage from '../views/homePage/homePage.vue'
import homePageDesktop from '../views/desktop/homeDesktop/homeDesktop.vue'
// import listGamePlay from '../components/desktop/home/listGamePlay.vue'
import sportGame from '../components/desktop/game/sport/sportGame.vue'
import casinoGame from '../components/desktop/game/casino/casinoGame.vue'
import FishingGame from '../components/desktop/game/slot/FISHINGgame.vue'
import Keno from '../components/desktop/game/keno/keno.vue'
import Poker from '../components/desktop/game/poker/poker.vue'
import promotionDesktop from '../components/desktop/promotion/promotion.vue'
import movieDesktop from '../components/desktop/game/movie/movie.vue'
import infoDesktop from '../components/desktop/loginMenu/info/info.vue'
import changePasswordDesktop from '../components/desktop/loginMenu/changePassword/changePassword.vue'
import depositDesktop from '../components/desktop/loginMenu/deposit/deposit.vue'
import depositViewDesktop from '../components/desktop/loginMenu/deposit/depositView.vue'
import withdrawViewDesktop from '../components/desktop/loginMenu/withdraw/withdrawView.vue'
import betReportViewDesktop from '../components/desktop/loginMenu/betReport/betReportView.vue'
import statementDesktop from '../components/desktop/loginMenu/statement/statement.vue'
import turnOverDesktop from '../components/desktop/loginMenu/turnOver/turnOver.vue'
import pageMovie from '../components/desktop/game/movie/pageMovie.vue'
import listSlotGameDesktop from '../components/desktop/game/slot/listSlotGame.vue'
import casinoListGameDesktop from '../components/desktop/game/casino/casinoListGame.vue'
// import depositListDesktop from '../components/desktop/loginMenu/deposit/depositList.vue'
import typeGameSlot from '../components/desktop/game/slot/typeGameSlot.vue'
import scoreball from '../components/desktop/ball/ball.vue'
import steamBall from '../components/desktop/ball/steamBall.vue'
// ------------------------------------- mobile --------------------------
import pageMobile from '../views/homePage/homeMobile.vue'
import listGameMobile from '../views/mobile/listgame/listGame.vue'
import promotion from '../components/mobile/promotion/promotionPage.vue'
import languaage from '../views/mobile/languageEmpty/emptyPage.vue'
import userInfo from '../views/mobile/userPage/userPage.vue'
import casinoPage from '../components/mobile/listgame/casinoLivePage.vue'
import slot from '../components/mobile/listgame/slotPage.vue'
import fishing from '../components/mobile/listgame/fishingPage.vue'
import movie from '../components/mobile/listgame/moviePage.vue'
import slotGame from '../components/mobile/listgame/slotGame.vue'
import casinogame from '../components/mobile/listgame/casinoGame.vue'
import sportmain from '../components/desktop/game/sport/sportmain.vue'
import menuuserinfo from '../components/desktop/toolbar/menuUserInfo.vue'
import userinfonew from '../components/desktop/toolbar/userInfoNew.vue'
import statementnew from '../components/desktop/toolbar/statement.vue'
import turnovernew from '../components/desktop/toolbar/turnovernew.vue'
import changepass from '../components/desktop/toolbar/userCpass.vue'
import skywindcasino from '../components/desktop/game/casino/casinoSkywind.vue'
// import landpage from '../views/firstpage/firstpage.vue'

Vue.use(VueAxios, axios)
Vue.use(VueRouter)

// function delaySayHello () {
//   return axios.get(
//     configuregeneral.urlService + 'getDataAgentLogin',
//     {
//       headers: {
//         'Content-Type': 'application/json',
//         // eslint-disable-next-line
//         'authorization': store.state.postAuth.dataAccessToken
//       }
//     }
//   )
// }

const routes = [
  {
    path: '/changepass/',
    name: 'changepass',
    component: changepass,
    meta: {
      requiresAuth: true,
      roles: true,
      transition: 'fade',
      typePage: 2
    }
  },
  {
    path: '/turnovernew/',
    name: 'turnovernew',
    component: turnovernew,
    meta: {
      requiresAuth: true,
      roles: true,
      transition: 'fade',
      typePage: 2
    }
  },
  {
    path: '/statementnew/',
    name: 'statementnew',
    component: statementnew,
    meta: {
      requiresAuth: true,
      roles: true,
      transition: 'fade',
      typePage: 2
    }
  },
  {
    path: '/userinfonew/',
    name: 'userinfonew',
    component: userinfonew,
    meta: {
      requiresAuth: true,
      roles: true,
      transition: 'fade',
      typePage: 2
    }
  },
  {
    path: '/menuuserinfo/',
    name: 'menuuserinfo',
    component: menuuserinfo,
    meta: {
      requiresAuth: true,
      roles: true,
      transition: 'fade',
      typePage: 2
    }
  },
  {
    path: '/dashboard/',
    name: 'dashboard',
    component: dashboard,
    meta: {
      requiresAuth: true,
      roles: true,
      transition: 'fade'
    }
  },
  {
    path: '/Movie/:name',
    name: 'pageMovie',
    component: pageMovie,
    // props: {
    //   items: 'items'
    // },
    meta: {
      requiresAuth: false,
      roles: true,
      transition: 'fade'
    }
  },
  {
    path: '/steamSport/:StreamName/:typeSport/:leage/:Opp1/:Opp2',
    name: 'steamBall',
    component: steamBall,
    // props: {
    //   items: 'items'
    // },
    meta: {
      requiresAuth: true,
      roles: true,
      transition: 'fade'
    }
  },
  {
    path: '/login',
    name: 'login',
    component: login,
    meta: {
      requiresAuth: false,
      roles: true,
      transition: 'fade'
    }
    // beforeEnter (to, from, next) {
    //   delaySayHello().then(data => {
    //     if (data.data.data.permission_add_agent === false) {
    //       next('/dashboard/')
    //     } else {
    //       next()
    //     }
    //   })
    // }
  },
  // ----------------------------- uuid----------------------------------
  {
    path: '/mainpage/:member_uuid',
    name: 'homePage',
    component: homePage,
    meta: {
      requiresAuth: false,
      roles: true,
      transition: 'fade'
    },
    children: [
      {
        path: '/mainpage/:member_uuid',
        name: 'homePage',
        component: homePageDesktop,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      }
    ]
  },
  // ----------------------------- uuid + providerID----------------------------------
  // {
  //   path: '/:member_uuid/:providerId',
  //   name: 'homePage',
  //   component: homePage,
  //   meta: {
  //     requiresAuth: false,
  //     roles: true,
  //     transition: 'fade'
  //   },
  //   children: [
  //     {
  //       path: '/:member_uuid/:providerId',
  //       name: 'homePage',
  //       component: homePageDesktop,
  //       meta: {
  //         requiresAuth: false,
  //         roles: true,
  //         transition: 'fade'
  //       }
  //     }
  //   ]
  // },
  // -----------------------------------------------------
  // {
  //   path: '/',
  //   name: 'landpage',
  //   component: landpage,
  //   meta: {
  //     requiresAuth: false,
  //     roles: true,
  //     transition: 'fade'
  //   }
  // },
  {
    path: '/mainpage',
    name: 'homePage',
    component: homePage,
    meta: {
      requiresAuth: false,
      roles: true,
      transition: 'fade'
    },
    children: [
      // -------------------------------------------------------------
      {
        path: '/mainpage',
        name: 'homePage',
        component: homePageDesktop,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/Slot/typeGameSlot',
        name: 'typeGameSlot',
        component: typeGameSlot,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/Slot/listSlotGame/:apiName/:keyLink/:name',
        name: 'listSlotGameDesktop',
        component: listSlotGameDesktop,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      // -------------------- member_uuid -----------------------------------------
      {
        path: '/desktop/Slot/listSlotGame/:apiName/:keyLink/:name/:member_uuid',
        name: 'listSlotGameDesktop',
        component: listSlotGameDesktop,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      // -------------------- end member_uuid -----------------------------------------
      {
        path: '/desktop/Fish/:keyLink/:name',
        name: 'FishingGame',
        component: FishingGame,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/Fish',
        name: 'FishingGame',
        component: FishingGame,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/sportmain',
        name: 'sportmain',
        component: sportmain,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/sportGame',
        name: 'sportGame',
        component: sportGame,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      // -------------------- member_uuid -----------------------------------------
      {
        path: '/desktop/sportGame/:member_uuid',
        name: 'sportGame',
        component: sportGame,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      // -------------------- end member_uuid -----------------------------------------
      {
        path: '/desktop/casinoGame',
        name: 'casinoGame',
        component: casinoGame,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      // -------------------- member_uuid -----------------------------------------
      {
        path: '/desktop/casinoGame/:member_uuid',
        name: 'casinoGame',
        component: casinoGame,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      // -------------------- end member_uuid -----------------------------------------
      {
        path: '/desktop/casinoListGame/:apiName/:keyLink/:name',
        name: 'casinoListGameDesktop',
        component: casinoListGameDesktop,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/skywindcasino/:apiName/:keyLink/:name',
        name: 'skywindcasinoDesktop',
        component: skywindcasino,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/register',
        name: 'register',
        component: register,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/Keno',
        name: 'Keno',
        component: Keno,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/Poker',
        name: 'Poker',
        component: Poker,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/promotionDesktop',
        name: 'promotionDesktop',
        component: promotionDesktop,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/movieDesktop',
        name: 'movieDesktop',
        component: movieDesktop,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/infoDesktop',
        name: 'infoDesktop',
        component: infoDesktop,
        meta: {
          requiresAuth: true,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/changePasswordDesktop',
        name: 'changePasswordDesktop',
        component: changePasswordDesktop,
        meta: {
          requiresAuth: true,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/depositDesktop',
        name: 'depositDesktop',
        component: depositDesktop,
        meta: {
          requiresAuth: true,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/depositViewDesktop',
        name: 'depositViewDesktop',
        component: depositViewDesktop,
        meta: {
          requiresAuth: true,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/withdrawViewDesktop',
        name: 'withdrawViewDesktop',
        component: withdrawViewDesktop,
        meta: {
          requiresAuth: true,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/betReportViewDesktop',
        name: 'betReportViewDesktop',
        component: betReportViewDesktop,
        meta: {
          requiresAuth: true,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/statementDesktop',
        name: 'statementDesktop',
        component: statementDesktop,
        meta: {
          requiresAuth: true,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/turnOverDesktop',
        name: 'turnOverDesktop',
        component: turnOverDesktop,
        meta: {
          requiresAuth: true,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/desktop/scoreball',
        name: 'scoreball',
        component: scoreball,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      }
    ]
  },
  {
    path: '/mobile',
    name: 'pageMobile',
    component: pageMobile,
    meta: {
      requiresAuth: false,
      roles: true,
      transition: 'fade'
    },
    children: [
      {
        path: '/mobile',
        name: 'listGameMobile',
        component: listGameMobile,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/languaage',
        name: 'languaage',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/promotion',
        name: 'promotion',
        component: promotion,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/contactPage',
        name: 'contactPage',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/userInfo',
        name: 'userInfo',
        component: userInfo,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/deposit',
        name: 'deposit',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/withdraw',
        name: 'withdraw',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/basicinfo',
        name: 'basicinfo',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/changepass',
        name: 'changepass',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/achievement',
        name: 'achievement',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/achievementreport',
        name: 'achievementreport',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/tranfermobile',
        name: 'tranfermobile',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/betreport',
        name: 'betreport',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/statement',
        name: 'statement',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/bonus',
        name: 'bonus',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/turnover',
        name: 'turnover',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/casinolive',
        name: 'casinolive',
        component: casinoPage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/casinogame/:apilink/:fullname/:keylink',
        name: 'casinogame',
        component: casinogame,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/slot',
        name: 'slot',
        component: slot,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/slot/:apilink/:fullname/:keylink',
        name: 'slotGame',
        component: slotGame,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/fishing',
        name: 'fishing',
        component: fishing,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/movie',
        name: 'movie',
        component: movie,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/listdeposit',
        name: 'listdeposit',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      },
      {
        path: '/mobile/listwithdraw',
        name: 'listwithdraw',
        component: languaage,
        meta: {
          requiresAuth: false,
          roles: true,
          transition: 'fade'
        }
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  routes: [
    ...routes, {
      path: '*',
      redirect: '/mainpage'
      // redirect: '/'
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(
          {
            x: 0,
            y: 0
          }
        )
      }, 500)
    })
  }
})
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    var sessionLogin = window.sessionStorage.getItem(
      configuregeneral.keySessionStorage +
      '_token'
    )
    if (!sessionLogin || sessionLogin === 'null') {
      next({
        path: '/mainpage',
        // path: '/',
        query: {
          redirect: to.fullPath
        }
      })
    } else {
      next()
    }
  } else {
    next()
  }
  if (to.name === 'login') {
    const token = window.sessionStorage.getItem(
      configuregeneral.keySessionStorage +
      '_token'
    )
    if (token) {
      next({
        path: '/mainpage'
        // path: '/'
      })
    } else {
      next()
    }
  }
  if (store.state.language.language && store.state.language.language !== i18n.locale) {
    i18n.locale = store.state.language.language
    next()
  } else if (!store.state.language.language) {
    store.dispatch('language/setLanguage', navigator.languages)
      .then(() => {
        i18n.locale = store.state.language.language
        next()
      })
  } else {
    next()
  }
})

export default router
