import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import VueCurrencyFilter from 'vue-currency-filter'
import Vuelidate from 'vuelidate'
import Snotify from 'vue-snotify'
import VueMeta from 'vue-meta'
import moment from 'moment'
import { i18n } from './plugins/i18n.js'
import VuePageTransition from 'vue-page-transition'
import 'vue-snotify/styles/material.css'
import MarqueeText from 'vue-marquee-text-component'
import VueClipboard from 'vue-clipboard2'
import vueFlvPlayer from 'vue-flv-player'
import VueIframe from 'vue-iframes'

Vue.component('marquee-text', MarqueeText)
Vue.use(Snotify)
Vue.use(Vuelidate)
Vue.use(VueMeta)
Vue.use(i18n)
Vue.use(VuePageTransition)
Vue.use(VueClipboard)
Vue.use(VueIframe)
Vue.use(vueFlvPlayer)

Vue.config.productionTip = false
Vue.use(VueCurrencyFilter,
  {
    symbol: '',
    thousandsSeparator: ',',
    fractionCount: 2,
    fractionSeparator: '.',
    symbolPosition: 'back',
    symbolSpacing: true
  }
)

Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(String(value)).format('MM/DD/YYYY hh:mm')
  }
})

new Vue({
  i18n,
  router,
  store,
  vuetify,
  beforeCreate () {
    Vue.$snotify = this.$snotify
  },
  render: h => h(App)
}).$mount('#app')
